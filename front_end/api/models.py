from django.db import models
from django.forms import DateField
from matplotlib.pyplot import title
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.timezone import now
   
class Filiere(models.Model):
    fil_name = models.CharField(max_length=200)

    def __str__(self):
        return self.fil_name
    
    

    
class Commission(models.Model):
    com_name = models.CharField(max_length=200)

    def __str__(self):
        return self.com_name
    
class User(AbstractUser):
    ide = models.IntegerField(unique=True)
    profile_photo = models.ImageField(verbose_name='Photo de profil')
    
    filiere = models.ForeignKey(Filiere, on_delete=models.CASCADE,default=1)
    commission = models.ForeignKey(Commission, on_delete=models.CASCADE,default=1)
 
    # def __str__(self):
    #     return f"{self.name} {self.username}"
    

class Publication(models.Model):
    title = models.CharField(max_length=200)
    texte = models.TextField()
    updated=models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='images/')

    def __str__(self):
        return self.title


class Notification(models.Model):
    typ = models.IntegerField()#Ya trois type de notification 1,2,3,4 respectivement acceuil,publication,commentaire,aime,info
    time = models.DateField(auto_now_add=True,blank=True)
    content = models.CharField(max_length=200)
    publication = models.ForeignKey(Publication, on_delete=models.CASCADE)
    user = models.ForeignKey(User,default=1, on_delete=models.CASCADE)
    read = models.BooleanField(default=False)


    # def __str__(self):
    #     return self.typ    

class Commentaire(models.Model): 
    title = models.TextField()
    time = models.DateField(auto_now_add=True,blank=True)
    publication = models.ForeignKey(Publication, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
    
class Reply(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    comment = models.ForeignKey(Commentaire, on_delete=models.CASCADE)
    title = models.TextField()
    time = models.DateField(auto_now_add=True)

    def __str__(self):
        return f"User={self.user.username}||comment={self.comment}"
    
    
class Like(models.Model):
    publication = models.ForeignKey(Publication, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    like = models.BooleanField(default=False)

    def __str__(self):
        return f"Post={self.publication.id}||User={self.user.username}||Like={self.like}"
