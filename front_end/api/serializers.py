from rest_framework.serializers import ModelSerializer
from .models import *
from rest_framework.authtoken.models import Token

class UserSerializers(ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username','first_name','last_name', 'password', 'email','profile_photo','filiere','commission','ide']
        extra_kwargs = {'password': {"write_only": True, 'required': True}}

        depth =1

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        Token.objects.create(user=user)
        return user
    
    
class FiliereSerializer(ModelSerializer):
    class Meta:
        model = Filiere
        fields = '__all__'
        
        
class CommissionSerializer(ModelSerializer):
    class Meta:
        model = Commission
        fields = '__all__'


class PublicationSerializer(ModelSerializer):
    class Meta:
        model = Publication
        fields = '__all__'
        # depth =1
        
    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['user'] = UserSerializers(instance.user).data
        return response
    
    
class NotificationSerializer(ModelSerializer):
    class Meta:
        model = Notification
        fields = '__all__'
        
        
    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['user'] = UserSerializers(instance.user).data
        return response
        
        
class CommentaireSerializer(ModelSerializer):
    class Meta:
        model = Commentaire
        fields = '__all__'
        
        
    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['user'] = UserSerializers(instance.user).data
        return response
        
        
class ReplySerializer(ModelSerializer):
    class Meta:
        model = Reply
        fields = "__all__"
        # depth = 1

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['user'] = UserSerializers(instance.user).data
        return response