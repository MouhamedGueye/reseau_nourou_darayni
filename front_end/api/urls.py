from django.urls import path
from .views import *
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
   path('posts/',PublicationView.as_view()),
   path('addlike/', AddALike.as_view()),
   path('addcomment/', AddComment.as_view()),
   path('addreply/', AddReply.as_view()),
   path('initParam/', UserParametre.as_view()),
   path('notifications/', NotificationVieuw.as_view()),
   path('login/', obtain_auth_token),
]