from django.contrib import admin
from .models import *

admin.site.register(Commission)
admin.site.register(Filiere)
admin.site.register(User)
admin.site.register(Publication)
admin.site.register(Commentaire)
admin.site.register(Like)
admin.site.register(Reply)
admin.site.register(Notification)
# Register your models here.
