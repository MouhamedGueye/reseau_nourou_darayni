from rest_framework.views import APIView
from rest_framework.response import Response

from .models import *
from .serializers import *


from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication

class PublicationView(APIView):
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication, ]
    
    def get(self, request):
        query = Publication.objects.all()
        serializer = PublicationSerializer(query, many=True)
        data = []
        for publication in serializer.data:
            publication_like = Like.objects.filter(
                publication=publication['id']).filter(like=True).count()
            mylike = Like.objects.filter(publication=publication['id']).filter(
                user=request.user).first()
            if mylike:
                publication['like'] = mylike.like
            else:
                publication['like'] = False
            publication['totallike'] = publication_like
            comment_query = Commentaire.objects.filter(
                publication=publication['id']).order_by('-id')
            comment_serializer = CommentaireSerializer(comment_query, many=True)
            comment_data = []
            for comment in comment_serializer.data:
                reply_query = Reply.objects.filter(comment=comment['id'])
                reply_serializer = ReplySerializer(reply_query, many=True)
                comment['reply'] = reply_serializer.data
                comment_data.append(comment)
            publication['comment'] = comment_data
            
            data2=[]
            query2 = request.user.notification_set.all()
            
            notifications = NotificationSerializer(query2, many=True)
            for noti in notifications.data:
                data2.append(noti)
                
            publication['notification']=data2
            # user_obj=User.objects.filter(username=request.user)
            
            # publication['user']=UserSerializers(request.user,many=False).data
            # up = User.objects.filter(
            #     publication=publication['user'])
            # publication['user'] = up
            data.append(publication)
            # users = User.objects.all()
            # usersSerialize = UserSerializers(up,many=True)
            # data.append(publication)
            # users = User.objects.all()
            # usersSerializes = UserSerializers(users,many=True)
            
        return Response(data)
    
    
    
class AddALike(APIView):
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication, ]

    def post(self, request):
        try:
            data = request.data
            c_user = request.user
            post_id = data['id']
            post_obj = Publication.objects.get(id=post_id)
            like_obj = Like.objects.filter(
                publication=post_obj).filter(user=c_user).first()
            if like_obj:
                old_like = like_obj.like
                like_obj.like = not old_like
                like_obj.save()
            else:
                Like.objects.create(
                    publication=post_obj,
                    user=c_user,
                    like=True,
                )
            response_msg = {'error': False}
        except:
            response_msg = {'error': True}
        return Response(response_msg)


class AddComment(APIView):
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication, ]

    def post(self, request):
        try:
            c_user = request.user
            data = request.data
            post_id = data['postid']
            comment_text = data['comment']
            print(c_user)
            print(post_id)
            print(comment_text)
            post_obj = Publication.objects.get(id=post_id)
            Commentaire.objects.create(
                user=c_user,
                publication=post_obj,
                title=comment_text,
            )
            response_msg = {'error': False, "postid": post_id}
        except:
            response_msg = {'error': True}
        return Response(response_msg)


class AddReply(APIView):
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication, ]

    def post(self, request):
        try:
            c_user = request.user
            data = request.data
            comment_id = data['commentid']
            comment_obj = Commentaire.objects.get(id=comment_id)
            replytext = data['replytext']
            Reply.objects.create(
                user=c_user,
                comment=comment_obj,
                title=replytext,
            )
            response_msg = {'error': False}
        except:
            response_msg = {'error': False}
        return Response(response_msg)
    
class UserParametre(APIView):
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication, ]

    def get(self, request):
        data=[]
        try:
            c_user = request.user
            userr=UserSerializers(request.user,many=False).data
            response_msg = {'error': False} 
        except:
            response_msg = {'error': False}
        return Response(userr)
    
    
class NotificationVieuw(APIView):
    permission_classes = [IsAuthenticated, ]
    authentication_classes = [TokenAuthentication, ]

    def get(self, request):
        data=[]
        try:
            c_user = request.user
            query  = c_user.notification_set.all()
            print(query)
            notifications = NotificationSerializer(query, many=True)
            for noti in notifications.data:
                # print("================")
                data.append(noti)
            # print(notifications_query.count())
            # notification=NotificationSerializer(notifications_query,many=True).data
            response_msg = {'error': True} 
        except:
            response_msg = {'error': False}
        return Response(data)


class Registernow(APIView):
    def post(sefl, request):
        serializers = UserSerializers(data=request.data)
        if serializers.is_valid():
            serializers.save()
            return Response({'error': False})
        return Response({'error': True})