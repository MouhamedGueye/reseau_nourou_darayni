class Publication {
  late int id;
  late String title;
  late String texte;
  late String updated;
  late String created;
  late String image;
  late User user;
  late bool like;
  late int totallike;
  late List<Comment> comment;
  late List<Notifications> notification;

  Publication(
      this.id,
      this.title,
      this.texte,
      this.updated,
      this.created,
      this.image,
      this.user,
      this.like,
      this.totallike,
      this.comment,
      this.notification);

  Publication.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    texte = json['texte'];
    updated = json['updated'];
    created = json['created'];
    image = json['image'];
    user = new User.fromJson(json['user']);
    like = json['like'];
    totallike = json['totallike'];
    if (json['comment'] != null) {
      comment = <Comment>[];
      json['comment'].forEach((v) {
        comment.add(new Comment.fromJson(v));
      });
    }
    if (json['notification'] != null) {
      notification = <Notifications>[];
      json['notification'].forEach((v) {
        notification.add(new Notifications.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['texte'] = this.texte;
    data['updated'] = this.updated;
    data['created'] = this.created;
    data['image'] = this.image;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['like'] = this.like;
    data['totallike'] = this.totallike;
    if (this.comment != null) {
      data['comment'] = this.comment.map((v) => v.toJson()).toList();
    }
    if (this.notification != null) {
      data['notification'] = this.notification.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class User {
  int? id;
  String? username;
  String? firstName;
  String? lastName;
  String? email;
  String? profilePhoto;
  Filiere? filiere;
  Commission? commission;

  User(this.id, this.username, this.firstName, this.lastName, this.email,
      this.profilePhoto, this.filiere, this.commission);

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    email = json['email'];
    profilePhoto = json['profile_photo'];
    filiere =
        json['filiere'] != null ? new Filiere.fromJson(json['filiere']) : null;
    commission = json['commission'] != null
        ? new Commission.fromJson(json['commission'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['profile_photo'] = this.profilePhoto;
    if (this.filiere != null) {
      data['filiere'] = this.filiere!.toJson();
    }
    if (this.commission != null) {
      data['commission'] = this.commission!.toJson();
    }
    return data;
  }
}

class Filiere {
  int? id;
  String? filName;

  Filiere({this.id, this.filName});

  Filiere.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    filName = json['fil_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['fil_name'] = this.filName;
    return data;
  }
}

class Commission {
  int? id;
  String? comName;

  Commission({this.id, this.comName});

  Commission.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    comName = json['com_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['com_name'] = this.comName;
    return data;
  }
}

class Comment {
  int? id;
  String? title;
  String? time;
  int? publication;
  User? user;
  List<Reply>? reply;

  Comment(
      {this.id,
      this.title,
      this.time,
      this.publication,
      this.user,
      this.reply});

  Comment.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    time = json['time'];
    publication = json['publication'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    if (json['reply'] != null) {
      reply = <Reply>[];
      json['reply'].forEach((v) {
        reply!.add(new Reply.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['time'] = this.time;
    data['publication'] = this.publication;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    if (this.reply != null) {
      data['reply'] = this.reply!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Reply {
  int? id;
  String? title;
  String? time;
  User? user;
  int? comment;

  Reply({this.id, this.title, this.time, this.user, this.comment});

  Reply.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    time = json['time'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    comment = json['comment'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['time'] = this.time;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    data['comment'] = this.comment;
    return data;
  }
}

class Notifications {
  late int id;
  late int typ;
  late String time;
  late String content;
  late bool read;
  late int publication;
  late User user;

  Notifications(this.id, this.typ, this.time, this.content, this.read,
      this.publication, this.user);

  Notifications.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    typ = json['typ'];
    time = json['time'];
    content = json['content'];
    read = json['read'];
    publication = json['publication'];
    user = User.fromJson(json['user']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['typ'] = this.typ;
    data['time'] = this.time;
    data['content'] = this.content;
    data['read'] = this.read;
    data['publication'] = this.publication;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}
