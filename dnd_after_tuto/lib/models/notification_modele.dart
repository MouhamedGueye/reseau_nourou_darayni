import 'dart:convert';

class NotificationModele {
  String destinataire;
  String destinataireImages;
  String type;
  String date;
  String contenu;
  NotificationModele(this.destinataire, this.destinataireImages, this.type,
      this.date, this.contenu);

  // NotificationModele copyWith({
  //   String? destinataire,
  //   String? type,
  //   String? date,
  //   String? contenu,
  // }) {
  //   return NotificationModele(
  //     destinataire: destinataire ?? this.destinataire,
  //     type: type ?? this.type,
  //     date: date ?? this.date,
  //     contenu: contenu ?? this.contenu,
  //   );
  // }

  Map<String, dynamic> toMap() {
    return {
      'destinataire': destinataire,
      'type': type,
      'date': date,
      'contenu': contenu,
    };
  }

  // factory NotificationModele.fromMap(Map<String, dynamic> map) {
  //   return NotificationModele(
  //     destinataire: map['destinataire'] ?? '',
  //     type: map['type'] ?? '',
  //     date: map['date'] ?? '',
  //     contenu: map['contenu'] ?? '',
  //   );
  // }

  String toJson() => json.encode(toMap());

  // factory NotificationModele.fromJson(String source) =>
  //     NotificationModele.fromMap(json.decode(source));

  // @override
  // String toString() {
  //   return 'NotificationModele(destinataire: $destinataire, type: $type, date: $date, contenu: $contenu)';
  // }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is NotificationModele &&
        other.destinataire == destinataire &&
        other.type == type &&
        other.date == date &&
        other.contenu == contenu;
  }

  @override
  int get hashCode {
    return destinataire.hashCode ^
        type.hashCode ^
        date.hashCode ^
        contenu.hashCode;
  }
}
