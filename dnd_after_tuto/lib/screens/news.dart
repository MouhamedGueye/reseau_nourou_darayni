import 'package:dnd_after_tuto/constante.dart';
import 'package:dnd_after_tuto/main.dart';
import 'package:dnd_after_tuto/post_state.dart';
import 'package:dnd_after_tuto/models/publication_modele.dart';
import 'package:dnd_after_tuto/widgets/single_publication.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class News extends StatefulWidget {
  const News({Key? key}) : super(key: key);
  static const routeName = '/news';

  @override
  _NewsState createState() => _NewsState();
}

class _NewsState extends State<News> {
  bool _init = true;
  bool _isLoding = false;

  @override
  void didChangeDependencies() async {
    if (_init) {
      _isLoding =
          await Provider.of<PostState>(context, listen: false).getPostData();
      setState(() {});
    }
    _init = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    final List<Publication> _pub = Provider.of<PostState>(context).publication;
    bool _showComments = false;

    if (_isLoding == false || _pub == null)
      return Center(
        child: CircularProgressIndicator(),
      );
    else
      return ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: _pub.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            padding: EdgeInsets.only(top: 1),
            child: Column(
              children: [
                Divider(
                  height: 5,
                  color: Colors.grey,
                ),
                SinglePost(_pub[index]),
              ],
            ),
          );
        },
      );
  }
}
