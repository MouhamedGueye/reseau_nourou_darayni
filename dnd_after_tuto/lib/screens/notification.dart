import 'package:dnd_after_tuto/constante.dart';
import 'package:dnd_after_tuto/main.dart';
import 'package:dnd_after_tuto/models/notification_modele.dart';
import 'package:dnd_after_tuto/models/publication_modele.dart';
import 'package:dnd_after_tuto/post_state.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Notifications extends StatefulWidget {
  const Notifications({Key? key}) : super(key: key);

  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  bool _init = true;
  bool _isLoding = false;

  List<NotificationModele> _notifications = [
    NotificationModele('DND', 'images/img1.jpeg', 'Dahira', '15/04/1998',
        'Un evenement se prepare par exemple'),
    NotificationModele('DND', 'images/img1.jpeg', 'Dahira', '15/04/1998',
        'Un evenement se prepare par exemple'),
    NotificationModele('DND', 'images/img1.jpeg', 'Dahira', '15/04/1998',
        'Un evenement se prepare par exemple'),
    NotificationModele('DND', 'images/img1.jpeg', 'Dahira', '15/04/1998',
        'Un evenement se prepare par exemple'),
    NotificationModele('DND', 'images/img1.jpeg', 'Dahira', '15/04/1998',
        'Un evenement se prepare par exemple'),
    NotificationModele('DND', 'images/img1.jpeg', 'Dahira', '15/04/1998',
        'Un evenement se prepare par exemple'),
    NotificationModele('DND', 'images/img1.jpeg', 'Dahira', '15/04/1998',
        'Un evenement se prepare par exemple'),
    NotificationModele('DND', 'images/img1.jpeg', 'Dahira', '15/04/1998',
        'Un evenement se prepare par exemple'),
  ];

  @override
  void didChangeDependencies() async {
    if (_init) {
      _isLoding = await Provider.of<PostState>(context, listen: false)
          .getNotificationData();
      setState(() {});
    }
    _init = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    dynamic _notif = Provider.of<PostState>(context).notifs;
    // print(_notif);
    if (_isLoding == false || _notif == null)
      return Center(
        child: CircularProgressIndicator(),
      );
    else
      return ListView.builder(
          padding: const EdgeInsets.all(8),
          itemCount: _notif.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              // decoration: BoxDecoration(color: Colors.black),
              height: 120,
              child: Card(
                // color: Color.fromRGBO(168, 168, 168, 0.1),
                child: Row(
                  children: [
                    Container(
                        height: 100,
                        padding: EdgeInsets.all(2),
                        margin: EdgeInsets.only(right: 10),
                        width: 100,
                        decoration: BoxDecoration(
                          color: couleurPrincipale,
                          shape: BoxShape.circle,
                        ),
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(
                            "${backend}${_notif[index].user.profilePhoto}",
                          ),
                        )),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding:
                                EdgeInsets.only(left: 20, top: 10, bottom: 10),
                            child: Row(
                              children: [
                                Text(
                                  _notifications[index].destinataire + ' - ',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 20),
                                ),
                                Text(
                                  _notifications[index].type,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 20),
                                ),
                              ],
                            ),
                          ),
                          Text(
                            _notifications[index].contenu,
                            style: TextStyle(
                              color: Colors.grey[700],
                              fontSize: 15,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 5),
                            child: Text(
                              _notifications[index].date,
                              style: TextStyle(
                                color: Colors.grey[700],
                                fontSize: 15,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      child: Icon(
                        Icons.check,
                        color: Colors.green,
                      ),
                    )
                  ],
                ),
              ),
            );
          });
  }
}
