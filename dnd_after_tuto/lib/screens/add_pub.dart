import 'package:dnd_after_tuto/main.dart';
import 'package:flutter/material.dart';

class AddPub extends StatelessWidget {
  const AddPub({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              child: ImageSection('images/img4.jpeg'),
            ),
            Escpacement(),
            EvenementInput(),
            Escpacement(),
            IllustrationInput(),
            Escpacement(),
            ButtonAdd(context)
          ],
        ),
      ),
    );
  }

  Widget Escpacement() => const SizedBox(
        height: 10,
      );

  Widget ImageSection(image) => Container(
        padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
        margin: EdgeInsets.only(top: 10),
        decoration: const BoxDecoration(
          // color: Colors.red,
          border: Border(
            top: BorderSide(
              color: Colors.black,
              width: 0.1,
            ),
            bottom: BorderSide(
              color: Colors.black,
              width: 0.1,
            ),
            left: BorderSide(
              color: Colors.black,
              width: 0.1,
            ),
            right: BorderSide(
              color: Colors.black,
              width: 0.1,
            ),
          ),
        ),
        child: Image.asset(
          image,
          fit: BoxFit.cover,
        ),
      );

  Widget EvenementInput() => TextFormField(
      decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: couleurPrincipale, width: 2),
            borderRadius: BorderRadius.circular(20.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            borderSide: BorderSide(color: couleurPrincipale, width: 2),
          ),
          hintText: 'Type d\'evenement',
          hintStyle: const TextStyle(
            color: Colors.grey,
            fontSize: 20,
          ),
          filled: true,
          fillColor: Colors.white));

  Widget IllustrationInput() => Container(
        child: TextFormField(
            decoration: InputDecoration(
                contentPadding: EdgeInsets.all(10),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: couleurPrincipale, width: 2),
                  borderRadius: BorderRadius.circular(20.0),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20.0),
                  borderSide: BorderSide(color: couleurPrincipale, width: 2),
                ),
                hintText: 'Type d\'evenement',
                hintStyle: const TextStyle(
                  color: Colors.grey,
                  fontSize: 20,
                ),
                filled: true,
                fillColor: Colors.white)),
      );

  Widget ButtonAdd(context) => Container(
        width: MediaQuery.of(context).size.width - 20,
        height: 40,
        decoration: BoxDecoration(
          color: couleurPrincipale,
          borderRadius: BorderRadius.circular(20.0),
          border: const Border(
            top: BorderSide(width: 2.0, color: Colors.white),
            bottom: BorderSide(width: 2.0, color: Colors.white),
            right: BorderSide(width: 2.0, color: Colors.white),
            left: BorderSide(width: 2.0, color: Colors.white),
          ),
        ),
        child: const TextButton(
          child: Text(
            'Publier',
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
          onPressed: null,
        ),
      );
}
