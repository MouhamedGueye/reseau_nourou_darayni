import 'package:dnd_after_tuto/screens/update_profile.dart';
import 'package:dnd_after_tuto/post_state.dart';
import 'package:dnd_after_tuto/screens/add_pub.dart';
import 'package:dnd_after_tuto/screens/detail_pub.dart';
import 'package:dnd_after_tuto/main.dart';
import 'package:dnd_after_tuto/screens/news.dart';
import 'package:dnd_after_tuto/screens/notification.dart';
import 'package:dnd_after_tuto/screens/profile.dart';
import 'package:dnd_after_tuto/widgets/main_drawer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:getwidget/getwidget.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);
  static const routeName = '/home';

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with TickerProviderStateMixin {
  late TabController _tabController;
  bool _init = true;

  @override
  void didChangeDependencies() {
    if (_init) {
      Provider.of<PostState>(context, listen: false).getPostData();
    }
    _init = false;
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: couleurPrincipale,
        leading: Container(
          margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
          child: CircleAvatar(
            backgroundImage: AssetImage('images/logo.jpg'),
          ),
        ),
        title: Text('Darara Nourou Darayni'),
        titleSpacing: 0.0,
        titleTextStyle: TextStyle(
          fontSize: 25,
          fontFamily: 'Tahoma',
        ),
        bottom: TabBar(
          indicator: BoxDecoration(
              border:
                  Border(bottom: BorderSide(width: 3, color: Colors.white))),
          controller: _tabController,
          tabs: const <Widget>[
            Tab(
              icon: Icon(Icons.article),
            ),
            Tab(icon: Icon(Icons.notifications)),
            Tab(
              icon: Icon(Icons.person),
            ),
          ],
        ),
      ),
      drawer: Drawer(
        // color: couleurPrincipale,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            UserAccountsDrawerHeader(
              decoration: BoxDecoration(
                color: couleurPrincipale,
              ),
              accountName: Text('mg'),
              accountEmail: Text('email'),
              currentAccountPicture: CircleAvatar(
                child: ClipOval(
                  child: Image.asset(
                    'images/img6.jpeg',
                    fit: BoxFit.cover,
                    width: 90,
                    height: 90,
                  ),
                ),
              ),
            ),
            ListTile(
              leading: Icon(Icons.person),
              title: Text('Profile'),
              onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Profile(),
                ),
              ),
            ),
            ListTile(
              leading: Icon(Icons.notifications),
              title: Text('Notification'),
              onTap: () => null,
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text('Parametre'),
              onTap: () => null,
            ),
            Divider(),
            ListTile(
              title: Text('Deconnexion'),
              leading: Icon(Icons.exit_to_app),
              onTap: () => null,
            ),
          ],
        ),
      ),
      body: Stack(fit: StackFit.passthrough, children: [
        TabBarView(
          controller: _tabController,
          children: [
            News(),
            Notifications(),
            Profile(),
          ],
        ),
      ]),
    );
  }
}
