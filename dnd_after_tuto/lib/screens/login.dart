import 'package:dnd_after_tuto/main.dart';
import 'package:dnd_after_tuto/screens/register.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:dnd_after_tuto/post_state.dart';

import 'package:dnd_after_tuto/screens/home.dart';

class Login extends StatefulWidget {
  static const routeName = '/login';

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String username = "";

  String password = "";

  final _keyform = GlobalKey<FormState>();

  void _loginNow() async {
    bool islogin = await Provider.of<PostState>(context, listen: false)
        .loginNow(username, password);
    if (!islogin) {
      Navigator.of(context).pushReplacementNamed(Home.routeName);
    } else {
      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Le login et le mot de passe ne correspondent pas"),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text("Ok"),
              ),
            ],
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          color: couleurPrincipale,
          padding: EdgeInsets.all(20),
          child: Container(
            decoration: BoxDecoration(
              color: Color.fromRGBO(0, 0, 0, 0.5),
              borderRadius: BorderRadius.circular(30.0),
              border: Border(
                top: BorderSide(width: 4.0, color: Colors.white),
                bottom: BorderSide(width: 4.0, color: Colors.white),
                right: BorderSide(width: 4.0, color: Colors.white),
                left: BorderSide(width: 4.0, color: Colors.white),
              ),
            ),
            padding: EdgeInsets.all(10),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 20),
                  height: 200,
                  width: 200,
                  child: CircleAvatar(
                    backgroundImage: AssetImage('images/logo.jpg'),
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                Form(
                  key: _keyform,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      TextFormField(
                        validator: (value) => username.isEmpty
                            ? "Veillez saisir le login svp !"
                            : null,
                        onChanged: (value) => username = value,
                        decoration: InputDecoration(
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20.0),
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            prefixIcon: const Icon(
                              Icons.person,
                              color: Colors.grey,
                            ),
                            hintText: 'Login',
                            hintStyle: const TextStyle(
                              color: Colors.grey,
                              fontSize: 20,
                            ),
                            filled: true,
                            fillColor: Colors.white),
                      ),
                      SizedBox(
                        height: 50.0,
                      ),
                      TextFormField(
                          validator: (value) => password.isEmpty
                              ? "Veillez saisir le mot de passe !"
                              : null,
                          onChanged: (value) => password = value,
                          obscureText: false,
                          decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20.0),
                                borderSide: BorderSide(color: Colors.grey),
                              ),
                              prefixIcon: const Icon(
                                Icons.password,
                                color: Colors.grey,
                              ),
                              hintText: 'Mot de passe',
                              hintStyle: const TextStyle(
                                color: Colors.grey,
                                fontSize: 20,
                              ),
                              filled: true,
                              fillColor: Colors.white)),
                      SizedBox(
                        height: 40.0,
                      ),
                      OutlinedButton(
                        onPressed: () {
                          if (_keyform.currentState!.validate()) {
                            _loginNow();
                          }
                        },
                        child: Text(
                          'Se connecter',
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                          ),
                        ),
                        style: OutlinedButton.styleFrom(
                            backgroundColor: couleurPrincipale,
                            padding: EdgeInsets.all(20),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                            side:
                                BorderSide(color: couleurPrincipale, width: 2)),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Register()),
                          );
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width - 20,
                          alignment: AlignmentGeometry.lerp(
                              Alignment.center, Alignment.center, 10),
                          child: const Text(
                            'Mot de passe oublié',
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Register()),
                          );
                        },
                        child: Text(
                          'S \'inscire',
                          style: TextStyle(fontSize: 20),
                        ),
                        style: OutlinedButton.styleFrom(
                            backgroundColor: Colors.grey,
                            padding: EdgeInsets.all(20),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20.0)),
                            side: BorderSide(color: Colors.white, width: 2)),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
