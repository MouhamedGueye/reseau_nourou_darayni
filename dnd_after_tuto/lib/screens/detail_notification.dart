import 'package:dnd_after_tuto/main.dart';
import 'package:flutter/material.dart';

class DetailPub extends StatefulWidget {
  const DetailPub({Key? key}) : super(key: key);

  @override
  _DetailPubState createState() => _DetailPubState();
}

class _DetailPubState extends State<DetailPub> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              HeaderSection('images/img1.jpeg', 'Mouhamed Gueye'),
              TextSection('mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm'),
              ImageSection('images/img6.jpeg'),
              ButtonSection(),
              CommentaireText(),
              SizedBox(
                height: 15,
              ),
              CommentaireItem(),
              LineBetweenPub(),
              CommentaireItem(),
              LineBetweenPub(),
            ],
          ),
        ),
      ),
    );
  }

  Widget LineBetweenPub() => Container(
        margin: EdgeInsets.only(top: 15, bottom: 15),
        height: 1,
        decoration: BoxDecoration(
            border: Border(
                bottom:
                    BorderSide(color: Color.fromRGBO(0, 0, 0, 0.4), width: 1))),
      );

  Widget CommentaireItem() => Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 70,
                padding: EdgeInsets.all(2),
                margin: EdgeInsets.only(right: 10),
                width: 70,
                decoration: BoxDecoration(
                  color: couleurPrincipale,
                  shape: BoxShape.circle,
                ),
                child: CircleAvatar(
                  backgroundImage: AssetImage('images/img2.jpeg'),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Mouhamed Gueye',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w600),
                ),
                Container(
                  child: Text(
                    'mmmmmmmmmmmmmmmmmmmm',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 15,
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      );

  Widget CommentaireText() => Container(
        padding: EdgeInsets.only(top: 20, left: 20),
        child: Text(
          'Commentaire',
          style: TextStyle(fontWeight: FontWeight.w700, fontSize: 20),
        ),
      );

  Widget HeaderSection(image, membre) => Row(children: [
        Container(
          height: 60,
          padding: EdgeInsets.all(2),
          // margin: EdgeInsets.only(right: 10),
          width: 60,
          decoration: BoxDecoration(
            color: couleurPrincipale,
            shape: BoxShape.circle,
          ),
          child: CircleAvatar(
            backgroundImage: AssetImage(image),
          ),
        ),
        Expanded(
          child: Container(
            child: Text(
              membre,
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
            ),
            height: 40,
            padding: EdgeInsets.only(top: 10, left: 10),
          ),
        )
      ]);
  Widget TextSection(illustration) => Container(
        padding: EdgeInsets.only(top: 5),
        // height: 60,
        child: Text(illustration),
      );

  Widget ImageSection(image) => Container(
        padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
        margin: EdgeInsets.only(top: 10),
        decoration: const BoxDecoration(
          // color: Colors.red,
          border: Border(
            top: BorderSide(
              color: Colors.black,
              width: 0.1,
            ),
            bottom: BorderSide(
              color: Colors.black,
              width: 0.1,
            ),
            left: BorderSide(
              color: Colors.black,
              width: 0.1,
            ),
            right: BorderSide(
              color: Colors.black,
              width: 0.1,
            ),
          ),
        ),
        child: Image.asset(
          image,
          fit: BoxFit.cover,
        ),
      );

  Widget ButtonSection() => Row(
        children: [
          Container(
            width: 2,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.fromLTRB(1, 1, 1, 1),
              margin: EdgeInsets.only(top: 10),
              // height: 30,
              // width: 100 - 2,
              decoration: const BoxDecoration(
                // color: Colors.red,
                border: Border(
                  top: BorderSide(
                    color: Colors.black,
                    width: 0.9,
                  ),
                  bottom: BorderSide(
                    color: Colors.black,
                    width: 0.9,
                  ),
                  left: BorderSide(
                    color: Colors.black,
                    width: 0.9,
                  ),
                  right: BorderSide(
                    color: Colors.black,
                    width: 0.9,
                  ),
                ),
              ),
              child: IconButton(
                icon: Icon(
                  Icons.offline_bolt,
                ),
                onPressed: () {},
              ),
            ),
          ),
          Container(
            width: 2,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.fromLTRB(1, 1, 1, 1),
              margin: EdgeInsets.only(top: 10),
              // height: 30,
              // width: 100 - 2,
              decoration: const BoxDecoration(
                // color: Colors.red,
                border: Border(
                  top: BorderSide(
                    color: Colors.black,
                    width: 0.9,
                  ),
                  bottom: BorderSide(
                    color: Colors.black,
                    width: 0.9,
                  ),
                  left: BorderSide(
                    color: Colors.black,
                    width: 0.9,
                  ),
                  right: BorderSide(
                    color: Colors.black,
                    width: 0.9,
                  ),
                ),
              ),
              child: IconButton(
                icon: Icon(
                  Icons.offline_bolt,
                ),
                onPressed: () {},
              ),
            ),
          ),
          Container(
            width: 2,
          ),
        ],
      );
}
