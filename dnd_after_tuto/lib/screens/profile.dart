import 'package:dnd_after_tuto/constante.dart';
import 'package:dnd_after_tuto/main.dart';
import 'package:dnd_after_tuto/models/publication_modele.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:getwidget/getwidget.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  LocalStorage storage = LocalStorage("usertoken");
  late User u;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    u = User.fromJson(storage.getItem('user'));
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: 300,
            width: 300,
            margin: EdgeInsets.only(top: 20),
            // height: 200,
            // width: 200,
            child: Image.network(
              "${backend}${u.profilePhoto}",
              // fit: BoxFit.cover,
            ),
            // GFAvatar(
            //   backgroundImage: NetworkImage("${backend}${u.profilePhoto}"),
            // )
            // Text("${backend}${u.profilePhoto}"),
          ),
          Container(
            // padding: EdgeInsets.only(top: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  " ${u.firstName} ${u.lastName}",
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.edit,
                    color: couleurPrincipale,
                  ),
                )
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: [
                LineBeetwen(),
                LabelLine('IDE : ${u.id}'),
                LineBeetwen(),
                LabelLine('Login : ${u.username}'),
                LineBeetwen(),
                LabelLine('Email : ${u.email} '),
                LineBeetwen(),
                LabelLine('Filiere : ${u.filiere!.filName.toString()} '),
                LineBeetwen(),
                LabelLine('Commission : ${u.commission!.comName.toString()}'),
                LineBeetwen(),
                // LabelLine('Niveau : ${u.!.comName.toString()}'),
                // LineBeetwen()
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget LabelLine(value) => Container(
        alignment: AlignmentGeometry.lerp(
            Alignment.centerLeft, Alignment.centerLeft, 12),
        padding: EdgeInsets.only(top: 10, left: 15),
        child: Text(
          '$value',
          style: TextStyle(
            color: Color.fromRGBO(0, 0, 0, 0.5),
            fontSize: 20,
            fontWeight: FontWeight.w500,
          ),
        ),
      );
  Widget LineBeetwen() => Container(
        padding: EdgeInsets.only(top: 15),
        decoration: BoxDecoration(
            border: Border(
                bottom:
                    BorderSide(color: Color.fromRGBO(0, 0, 0, 0.4), width: 1))),
      );
}
