import 'package:dnd_after_tuto/constante.dart';
import 'package:dnd_after_tuto/main.dart';
import 'package:dnd_after_tuto/models/publication_modele.dart';
import 'package:dnd_after_tuto/post_state.dart';
import 'package:dnd_after_tuto/screens/commentaire_list.dart';
import 'package:flutter/material.dart';
import 'package:getwidget/getwidget.dart';
import 'package:provider/provider.dart';

class DetailPub extends StatefulWidget {
  late Publication post;
  DetailPub(this.post);

  @override
  _DetailPubState createState() => _DetailPubState();
}

class _DetailPubState extends State<DetailPub> {
  bool _showComments = false;
  String commenttitle = '';
  final commentControler = TextEditingController();

  void _addComment() {
    if (commenttitle.length <= 0) {
      return;
    }
    Provider.of<PostState>(context, listen: false)
        .addcomment(widget.post.id.toInt(), commenttitle);
    commentControler.text = '';
    commenttitle = '';
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail de la publication"),
        backgroundColor: couleurPrincipale,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(children: [
                Container(
                  height: 60,
                  padding: EdgeInsets.all(2),
                  // margin: EdgeInsets.only(right: 10),
                  width: 60,
                  decoration: BoxDecoration(
                    color: couleurPrincipale,
                    shape: BoxShape.circle,
                  ),
                  child: CircleAvatar(
                    backgroundImage: AssetImage('images/img6.jpeg'),
                  ),
                ),
                Expanded(
                  child: Row(
                    children: [
                      Container(
                        child: Text(
                          widget.post.title.toString(),
                          style: TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 18),
                        ),
                        height: 40,
                        padding: EdgeInsets.only(top: 10, left: 10),
                      ),
                      Card(
                        child: Padding(
                          padding: const EdgeInsets.all(3.0),
                          child: Text('Evenement'),
                        ),
                      )
                    ],
                  ),
                )
              ]),
              TextSection(widget.post.texte),
              Card(
                child: Container(
                  height: 300,
                  width: 300,
                  padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                  margin: EdgeInsets.only(top: 10),
                  decoration: const BoxDecoration(
                    // color: Colors.red,
                    border: Border(
                      top: BorderSide(
                        color: Colors.black,
                        width: 0.1,
                      ),
                      bottom: BorderSide(
                        color: Colors.black,
                        width: 0.1,
                      ),
                      left: BorderSide(
                        color: Colors.black,
                        width: 0.1,
                      ),
                      right: BorderSide(
                        color: Colors.black,
                        width: 0.1,
                      ),
                    ),
                  ),
                  child: Image.network(
                    "${backend}${(widget.post.image.toString())}",
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: 3,
                  ),
                  Expanded(
                    child: ElevatedButton.icon(
                      style: TextButton.styleFrom(
                        backgroundColor: Colors.white,
                      ),
                      onPressed: () {
                        Provider.of<PostState>(context, listen: false)
                            .addlike(widget.post.id);
                      },
                      icon: Icon(
                        widget.post.like
                            ? Icons.favorite_border
                            : Icons.favorite,
                        color:
                            widget.post.like ? couleurPrincipale : Colors.grey,
                      ),
                      label: Text(
                        "J'aime(${widget.post.totallike})",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: widget.post.like
                              ? couleurPrincipale
                              : Colors.grey,
                          fontSize: 17,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: 5,
                  ),
                  Expanded(
                    child: ElevatedButton.icon(
                      style: TextButton.styleFrom(
                        backgroundColor: Colors.white,
                      ),
                      onPressed: () {
                        print(widget.post.comment.length);
                        setState(() {
                          _showComments = !_showComments;
                        });
                      },
                      icon: Icon(
                        Icons.comment,
                        color: Colors.grey,
                      ),
                      label: Text(
                        'Commentaire',
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: 3,
                  ),
                ],
              ),
              if (_showComments)
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Container(
                    child: TextField(
                      controller: commentControler,
                      onChanged: (v) {
                        setState(() {
                          commenttitle = v;
                        });
                      },
                      decoration: InputDecoration(
                        hintText: "Comment...",
                        suffix: IconButton(
                          onPressed: commenttitle.length <= 0
                              ? null
                              : () {
                                  _showComments = !_showComments;
                                  _addComment();
                                  GFToast.showToast(
                                      'Commentaire ajouter', context,
                                      toastPosition: GFToastPosition.BOTTOM,
                                      textStyle: TextStyle(
                                          fontSize: 16, color: GFColors.DARK),
                                      backgroundColor: GFColors.LIGHT,
                                      trailing: Icon(
                                        Icons.comment,
                                        color: GFColors.SUCCESS,
                                      ));
                                },
                          icon: Icon(Icons.send),
                          color: Colors.blue,
                        ),
                      ),
                    ),
                  )
                ]),
              SizedBox(
                height: 15,
              ),
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Voir les commentaires",
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                      IconButton(
                        icon: Icon(Icons.arrow_right, size: 35),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => CommentaireList(
                                    widget.post.comment, widget.post)),
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ),
              // CommentaireItem(),
              // LineBetweenPub(),
              // CommentaireItem(),
              // LineBetweenPub(),
            ],
          ),
        ),
      ),
    );
  }

  Widget LineBetweenPub() => Container(
        margin: EdgeInsets.only(top: 15, bottom: 15),
        height: 1,
        decoration: BoxDecoration(
            border: Border(
                bottom:
                    BorderSide(color: Color.fromRGBO(0, 0, 0, 0.4), width: 1))),
      );

  Widget CommentaireItem() => Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 70,
                padding: EdgeInsets.all(2),
                margin: EdgeInsets.only(right: 10),
                width: 70,
                decoration: BoxDecoration(
                  color: couleurPrincipale,
                  shape: BoxShape.circle,
                ),
                child: CircleAvatar(
                  backgroundImage: AssetImage('images/img2.jpeg'),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Mouhamed Gueye',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w600),
                ),
                Container(
                  child: Text(
                    'mmmmmmmmmmmmmmmmmmmm',
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 15,
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      );

  Widget CommentaireText() => Container(
        padding: EdgeInsets.only(top: 20, left: 20),
        child: Text(
          'Commentaire',
          style: TextStyle(fontWeight: FontWeight.w700, fontSize: 20),
        ),
      );

  Widget HeaderSection(image, membre) => Row(children: [
        Container(
          height: 60,
          padding: EdgeInsets.all(2),
          // margin: EdgeInsets.only(right: 10),
          width: 60,
          decoration: BoxDecoration(
            color: couleurPrincipale,
            shape: BoxShape.circle,
          ),
          child: CircleAvatar(
            backgroundImage: AssetImage(image),
          ),
        ),
        Expanded(
          child: Container(
            child: Text(
              membre,
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
            ),
            height: 40,
            padding: EdgeInsets.only(top: 10, left: 10),
          ),
        )
      ]);
  Widget TextSection(illustration) => Container(
        padding: EdgeInsets.only(top: 5),
        // height: 60,
        child: Text(illustration),
      );

  Widget ImageSection(image) => Container(
        padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
        margin: EdgeInsets.only(top: 10),
        decoration: const BoxDecoration(
          // color: Colors.red,
          border: Border(
            top: BorderSide(
              color: Colors.black,
              width: 0.1,
            ),
            bottom: BorderSide(
              color: Colors.black,
              width: 0.1,
            ),
            left: BorderSide(
              color: Colors.black,
              width: 0.1,
            ),
            right: BorderSide(
              color: Colors.black,
              width: 0.1,
            ),
          ),
        ),
        child: Image.asset(
          image,
          fit: BoxFit.cover,
        ),
      );

  Widget ButtonSection() => Row(
        children: [
          Container(
            width: 2,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.fromLTRB(1, 1, 1, 1),
              margin: EdgeInsets.only(top: 10),
              // height: 30,
              // width: 100 - 2,
              decoration: const BoxDecoration(
                // color: Colors.red,
                border: Border(
                  top: BorderSide(
                    color: Colors.black,
                    width: 0.9,
                  ),
                  bottom: BorderSide(
                    color: Colors.black,
                    width: 0.9,
                  ),
                  left: BorderSide(
                    color: Colors.black,
                    width: 0.9,
                  ),
                  right: BorderSide(
                    color: Colors.black,
                    width: 0.9,
                  ),
                ),
              ),
              child: IconButton(
                icon: Icon(
                  Icons.offline_bolt,
                ),
                onPressed: () {},
              ),
            ),
          ),
          Container(
            width: 2,
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.fromLTRB(1, 1, 1, 1),
              margin: EdgeInsets.only(top: 10),
              // height: 30,
              // width: 100 - 2,
              decoration: const BoxDecoration(
                // color: Colors.red,
                border: Border(
                  top: BorderSide(
                    color: Colors.black,
                    width: 0.9,
                  ),
                  bottom: BorderSide(
                    color: Colors.black,
                    width: 0.9,
                  ),
                  left: BorderSide(
                    color: Colors.black,
                    width: 0.9,
                  ),
                  right: BorderSide(
                    color: Colors.black,
                    width: 0.9,
                  ),
                ),
              ),
              child: IconButton(
                icon: Icon(
                  Icons.offline_bolt,
                ),
                onPressed: () {},
              ),
            ),
          ),
          Container(
            width: 2,
          ),
        ],
      );
}
