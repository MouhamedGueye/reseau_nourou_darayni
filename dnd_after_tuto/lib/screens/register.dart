import 'package:dnd_after_tuto/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Register extends StatelessWidget {
  const Register({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: couleurPrincipale,
        title: Text('S\'inscrire'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          color: couleurPrincipale,
          padding: EdgeInsets.all(20),
          child: Container(
            decoration: BoxDecoration(
              color: Color.fromRGBO(0, 0, 0, 0.5),
              borderRadius: BorderRadius.circular(30.0),
              border: Border(
                top: BorderSide(width: 4.0, color: Colors.white),
                bottom: BorderSide(width: 4.0, color: Colors.white),
                right: BorderSide(width: 4.0, color: Colors.white),
                left: BorderSide(width: 4.0, color: Colors.white),
              ),
            ),
            padding: EdgeInsets.all(10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 20),
                  height: 100,
                  width: 100,
                  child: CircleAvatar(
                    backgroundImage: AssetImage('images/logo.jpg'),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Form(
                    child: Column(
                  children: [
                    LoginInput(),
                    EscpacementInput(),
                    PasswordInput(),
                    EscpacementInput(),
                    PasswordInput(),
                    EscpacementInput(),
                    PrenomInput(),
                    EscpacementInput(),
                    NomInput(),
                    EscpacementInput(),
                    EmailInput(),
                    EscpacementInput(),
                    IDEInput(),
                    EscpacementInput(),
                    VerificationdInput(),
                    EscpacementInput(),
                    FiliereInput(),
                    EspacementAfterInput(),
                    ButtonSignUp(context),
                    EspacementAferLoginButton(),
                  ],
                ))
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget EspacementAferLoginButton() => const SizedBox(
        height: 10,
      );
  Widget ButtonSignUp(context) => Container(
        width: MediaQuery.of(context).size.width - 20,
        height: 40,
        decoration: BoxDecoration(
          color: couleurPrincipale,
          borderRadius: BorderRadius.circular(20.0),
          border: const Border(
            top: BorderSide(width: 2.0, color: Colors.white),
            bottom: BorderSide(width: 2.0, color: Colors.white),
            right: BorderSide(width: 2.0, color: Colors.white),
            left: BorderSide(width: 2.0, color: Colors.white),
          ),
        ),
        child: const TextButton(
          child: Text(
            'S\'inscrire',
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
          onPressed: null,
        ),
      );

  Widget EspacementAfterInput() => const SizedBox(
        height: 20,
      );
  Widget EscpacementInput() => const SizedBox(
        height: 10,
      );
  Widget LoginInput() => TextFormField(
      decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(20.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            borderSide: BorderSide(color: Colors.grey),
          ),
          hintText: 'Login',
          hintStyle: const TextStyle(
            color: Colors.grey,
            fontSize: 20,
          ),
          filled: true,
          fillColor: Colors.white));

  Widget PasswordInput() => TextFormField(
      decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(20.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            borderSide: BorderSide(color: Colors.grey),
          ),
          hintText: 'Mot de passe',
          hintStyle: const TextStyle(
            color: Colors.grey,
            fontSize: 20,
          ),
          filled: true,
          fillColor: Colors.white));

  Widget PrenomInput() => TextFormField(
      decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(20.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            borderSide: BorderSide(color: Colors.grey),
          ),
          hintText: 'Prenom',
          hintStyle: const TextStyle(
            color: Colors.grey,
            fontSize: 20,
          ),
          filled: true,
          fillColor: Colors.white));

  Widget NomInput() => TextFormField(
      decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(20.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            borderSide: BorderSide(color: Colors.grey),
          ),
          hintText: 'Nom',
          hintStyle: const TextStyle(
            color: Colors.grey,
            fontSize: 20,
          ),
          filled: true,
          fillColor: Colors.white));

  Widget EmailInput() => TextFormField(
      decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(20.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            borderSide: BorderSide(color: Colors.grey),
          ),
          hintText: 'Email',
          hintStyle: const TextStyle(
            color: Colors.grey,
            fontSize: 20,
          ),
          filled: true,
          fillColor: Colors.white));

  Widget IDEInput() => TextFormField(
      decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(20.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            borderSide: BorderSide(color: Colors.grey),
          ),
          hintText: 'IDE',
          hintStyle: const TextStyle(
            color: Colors.grey,
            fontSize: 20,
          ),
          filled: true,
          fillColor: Colors.white));

  Widget VerificationdInput() => TextFormField(
      obscureText: true,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(20.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            borderSide: BorderSide(color: Colors.grey),
          ),
          hintText: 'Verification',
          hintStyle: const TextStyle(
            color: Colors.grey,
            fontSize: 20,
          ),
          filled: true,
          fillColor: Colors.white));

  Widget FiliereInput() => TextFormField(
      decoration: InputDecoration(
          contentPadding: EdgeInsets.all(10),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(20.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20.0),
            borderSide: BorderSide(color: Colors.grey),
          ),
          hintText: 'Filiere',
          hintStyle: const TextStyle(
            color: Colors.grey,
            fontSize: 20,
          ),
          filled: true,
          fillColor: Colors.white));
}
