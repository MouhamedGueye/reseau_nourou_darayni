import 'package:dnd_after_tuto/main.dart';
import 'package:dnd_after_tuto/models/publication_modele.dart';
import 'package:dnd_after_tuto/post_state.dart';
import 'package:dnd_after_tuto/widgets/single_comments.dart';
import 'package:flutter/material.dart';
import 'package:getwidget/getwidget.dart';
import 'package:provider/provider.dart';

class CommentaireList extends StatefulWidget {
  List<Comment> lc;
  Publication post;
  CommentaireList(this.lc, this.post);

  @override
  _CommentaireListState createState() => _CommentaireListState();
}

class _CommentaireListState extends State<CommentaireList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Liste des commentaires"),
        backgroundColor: couleurPrincipale,
      ),
      body: ListView.builder(
          padding: const EdgeInsets.all(8),
          itemCount: widget.lc.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              // height: 50,
              child: Expanded(
                child: SingleComment(widget.lc[index]),
              ),
            );
          }),
    );
  }
}
