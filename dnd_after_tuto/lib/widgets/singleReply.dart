import 'package:dnd_after_tuto/models/publication_modele.dart';
import 'package:flutter/material.dart';

class SingleReply extends StatelessWidget {
  final Reply reply;
  SingleReply(this.reply);
  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "by ${reply.user} on ${reply.time}",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              reply.title.toString(),
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
