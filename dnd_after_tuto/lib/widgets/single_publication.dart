import 'package:dnd_after_tuto/constante.dart';
import 'package:dnd_after_tuto/main.dart';
import 'package:dnd_after_tuto/models/publication_modele.dart';
import 'package:dnd_after_tuto/post_state.dart';
import 'package:dnd_after_tuto/screens/commentaire_list.dart';
import 'package:dnd_after_tuto/screens/detail_pub.dart';
import 'package:flutter/material.dart';
import 'package:getwidget/getwidget.dart';
import 'package:provider/provider.dart';

class SinglePost extends StatefulWidget {
  final Publication post;
  SinglePost(this.post);

  @override
  _SinglePostState createState() => _SinglePostState();
}

class _SinglePostState extends State<SinglePost> {
  bool _showComments = false;
  String commenttitle = '';
  final commentControler = TextEditingController();

  void _addComment() {
    if (commenttitle.length <= 0) {
      return;
    }
    Provider.of<PostState>(context, listen: false)
        .addcomment(widget.post.id.toInt(), commenttitle);
    commentControler.text = '';
    commenttitle = '';
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(children: [
            Container(
              height: 60,
              padding: EdgeInsets.all(2),
              // margin: EdgeInsets.only(right: 10),
              width: 60,
              decoration: BoxDecoration(
                color: couleurPrincipale,
                shape: BoxShape.circle,
              ),
              child: CircleAvatar(
                backgroundImage: AssetImage('images/img6.jpeg'),
              ),
            ),
            Expanded(
              child: Row(
                children: [
                  Container(
                    child: Text(
                      widget.post.title.toString(),
                      style:
                          TextStyle(fontWeight: FontWeight.w600, fontSize: 18),
                    ),
                    height: 40,
                    padding: EdgeInsets.only(top: 10, left: 10),
                  ),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(3.0),
                      child: Text('Evenement'),
                    ),
                  )
                ],
              ),
            )
          ]),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              (widget.post.texte.toString().length > 100
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.only(top: 5),
                          // height: 60,
                          child: Text(
                            (widget.post.texte.toString().substring(0, 150)),
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                        ),
                        TextButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => DetailPub(
                                          widget.post,
                                        )),
                              );
                            },
                            child: Text(
                              'Voir plus',
                              style: TextStyle(
                                fontSize: 16,
                                color: couleurPrincipale,
                              ),
                            ))
                      ],
                    )
                  : Container(
                      padding: EdgeInsets.only(top: 5),
                      // height: 60,
                      child: Text(
                        (widget.post.texte.toString()),
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    )),
            ],
          ),
          Card(
            child: Container(
              height: 300,
              width: 300,
              padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
              margin: EdgeInsets.only(top: 10),
              decoration: const BoxDecoration(
                // color: Colors.red,
                border: Border(
                  top: BorderSide(
                    color: Colors.black,
                    width: 0.1,
                  ),
                  bottom: BorderSide(
                    color: Colors.black,
                    width: 0.1,
                  ),
                  left: BorderSide(
                    color: Colors.black,
                    width: 0.1,
                  ),
                  right: BorderSide(
                    color: Colors.black,
                    width: 0.1,
                  ),
                ),
              ),
              child: Image.network(
                "${backend}${(widget.post.image.toString())}",
                fit: BoxFit.cover,
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                width: 3,
              ),
              Expanded(
                child: ElevatedButton.icon(
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.white,
                  ),
                  onPressed: () {
                    GFToast.showToast(
                        widget.post.like ? 'Aime retirer' : 'Aime', context,
                        toastPosition: GFToastPosition.BOTTOM,
                        textStyle:
                            TextStyle(fontSize: 16, color: GFColors.DARK),
                        backgroundColor: GFColors.LIGHT,
                        trailing: Icon(
                          Icons.notifications,
                          color:
                              widget.post.like ? Colors.red : GFColors.SUCCESS,
                        ));
                    Provider.of<PostState>(context, listen: false)
                        .addlike(widget.post.id);
                  },
                  icon: Icon(
                    widget.post.like ? Icons.favorite_border : Icons.favorite,
                    color: widget.post.like ? couleurPrincipale : Colors.grey,
                  ),
                  label: Text(
                    "J'aime(${widget.post.totallike})",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: widget.post.like ? couleurPrincipale : Colors.grey,
                      fontSize: 17,
                    ),
                  ),
                ),
              ),
              Container(
                width: 5,
              ),
              Expanded(
                child: ElevatedButton.icon(
                  style: TextButton.styleFrom(
                    backgroundColor: Colors.white,
                  ),
                  onPressed: () {
                    print('mg');
                    setState(() {
                      _showComments = !_showComments;
                    });
                  },
                  icon: Icon(
                    Icons.comment,
                    color: Colors.grey,
                  ),
                  label: Text(
                    'Commentaire',
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                ),
              ),
              Container(
                width: 3,
              ),
            ],
          ),
          if (!_showComments)
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CommentaireList(
                            widget.post.comment,
                            widget.post,
                          )),
                );
              },
              child: Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.only(
                    top: 15,
                    bottom: 5,
                  ),
                  child: Text(
                    'Voir les commentaires',
                    style: TextStyle(
                      color: Colors.blue,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
              ),
            ),
          if (_showComments)
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: TextField(
                    controller: commentControler,
                    onChanged: (v) {
                      setState(() {
                        commenttitle = v;
                      });
                    },
                    decoration: InputDecoration(
                      hintText: "Comment...",
                      suffix: IconButton(
                        onPressed: commenttitle.length <= 0
                            ? null
                            : () {
                                _showComments = !_showComments;
                                _addComment();
                                GFToast.showToast(
                                    'Commentaire ajouter', context,
                                    toastPosition: GFToastPosition.BOTTOM,
                                    textStyle: TextStyle(
                                        fontSize: 16, color: GFColors.DARK),
                                    backgroundColor: GFColors.LIGHT,
                                    trailing: Icon(
                                      Icons.comment,
                                      color: GFColors.SUCCESS,
                                    ));
                              },
                        icon: Icon(Icons.send),
                        color: Colors.blue,
                      ),
                    ),
                  ),
                ),

                // Expanded(
                //   child: ListView.builder(
                //       padding: const EdgeInsets.all(8),
                //       itemCount: widget.post.comment.length,
                //       itemBuilder: (BuildContext context, int index) {
                //         return Container(
                //           padding: EdgeInsets.only(top: 1),
                //           child: Column(
                //             children: [
                //               Divider(
                //                 height: 5,
                //                 color: Colors.grey,
                //               ),
                //               // SingleComment(widget.post.comment[index]),
                //             ],
                //           ),
                //         );
                //       }),
                // )
              ],
            ),
        ],
      ),
    );
  }
}
