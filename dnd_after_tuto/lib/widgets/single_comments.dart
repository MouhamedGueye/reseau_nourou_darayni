import 'package:dnd_after_tuto/constante.dart';
import 'package:dnd_after_tuto/main.dart';
import 'package:dnd_after_tuto/models/publication_modele.dart';
import 'package:dnd_after_tuto/post_state.dart';
import 'package:dnd_after_tuto/widgets/singleReply.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:getwidget/getwidget.dart';

class SingleComment extends StatefulWidget {
  final Comment comment;
  SingleComment(this.comment);
  @override
  _SingleCommentState createState() => _SingleCommentState();
}

class _SingleCommentState extends State<SingleComment> {
  bool _showReply = false;
  final replycontroler = TextEditingController();
  String replytext = '';
  void _addreply() {
    if (replytext.length <= 0) {
      return;
    }
    Provider.of<PostState>(context, listen: false)
        .addreply(widget.comment.id!.toInt(), replytext);
    replycontroler.text = '';
    replytext = '';
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return GFCard(
      boxFit: BoxFit.cover,
      image: Image.asset('your asset image'),
      title: GFListTile(
        avatar: GFAvatar(
          size: 50,
          backgroundImage: NetworkImage(
              "${backend}${(widget.comment.user!.profilePhoto.toString())}"),
        ),
        title: Text(
            '${widget.comment.user!.firstName} ${widget.comment.user!.lastName}'),
        // subTitle: Text('Card Sub Title'),
      ),
      content: Text("${widget.comment.title}"),
      buttonBar: GFButtonBar(
        children: <Widget>[
          GFButton(
            icon: Icon(
              Icons.favorite,
              color: Colors.white,
            ),
            onPressed: () {},
            text: 'J\'aime',
          ),
          // GFButton(
          //   color: couleurPrincipale,
          //   onPressed: () {},
          //   text: 'Cancel',
          // ),
        ],
      ),
    );
  }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Padding(
//         padding: EdgeInsets.all(10),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Text(
//               "BY ${widget.comment.user} at ${widget.comment.title}",
//               style: TextStyle(
//                 fontWeight: FontWeight.bold,
//                 fontSize: 18,
//               ),
//             ),
//             Container(
//               margin: EdgeInsets.only(left: 10),
//               padding: EdgeInsets.all(4),
//               child: Text(
//                 widget.comment.title.toString(),
//                 style: TextStyle(
//                   fontSize: 16,
//                 ),
//               ),
//             ),
//             TextButton(
//               onPressed: () {
//                 setState(() {
//                   _showReply = !_showReply;
//                 });
//               },
//               child: Text("Reply(${widget.comment.reply!.length})"),
//             ),
//             if (_showReply)
//               Container(
//                 margin: EdgeInsets.only(left: 30),
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Container(
//                       child: TextField(
//                         controller: replycontroler,
//                         onChanged: (v) {
//                           setState(() {
//                             replytext = v;
//                           });
//                         },
//                         decoration: InputDecoration(
//                           hintText: "Reply",
//                           suffix: IconButton(
//                             onPressed: replytext.length <= 0
//                                 ? null
//                                 : () {
//                                     _addreply();
//                                   },
//                             icon: Icon(Icons.send),
//                             color: Theme.of(context).accentColor,
//                           ),
//                         ),
//                       ),
//                     ),
//                     if (widget.comment.reply!.length != 0)
//                       Expanded(
//                         child: ListView.builder(
//                             padding: const EdgeInsets.all(8),
//                             itemCount: widget.comment.reply!.length,
//                             itemBuilder: (BuildContext context, int index) {
//                               return Container(
//                                 padding: EdgeInsets.only(top: 1),
//                                 child: Column(
//                                   children: [
//                                     Divider(
//                                       height: 5,
//                                       color: Colors.grey,
//                                     ),
//                                     SingleReply(widget.comment.reply![index]),
//                                   ],
//                                 ),
//                               );
//                             }),
//                       )
//                   ],
//                 ),
//               ),
//           ],
//         ),
//       ),
//     );
//   }
}
