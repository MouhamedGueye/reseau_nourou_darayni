import 'dart:convert';

import 'package:dnd_after_tuto/models/publication_modele.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';

class PostState with ChangeNotifier {
  List<Publication> _publications = [];
  List<Notifications> _notification = [];
  LocalStorage storage = LocalStorage("usertoken");
  Future<bool> getPostData() async {
    try {
      var token = storage.getItem('token');
      String url = 'http://10.0.2.2:8000/api/posts/';
      http.Response response = await http.get(Uri.parse(url), headers: {
        'Authorization': 'token $token',
      });
      var data = json.decode(response.body) as List;
      List<Publication> temp = [];
      data.forEach((element) {
        Publication post = Publication.fromJson(element);
        temp.add(post);
      });
      _publications = temp;
      print(_publications);

      notifyListeners();
      getNotificationData();
      return true;
    } catch (e) {
      print("error getPostData");
      print(e);
      return false;
    }
  }

  Future<bool> getNotificationData() async {
    try {
      var token = storage.getItem('token');
      String url = 'http://10.0.2.2:8000/api/notifications/';
      http.Response response = await http.get(Uri.parse(url), headers: {
        'Authorization': 'token $token',
      });
      var data = json.decode(response.body) as List;
      List<Notifications> temp = [];
      data.forEach((element) {
        Notifications post = Notifications.fromJson(element);
        temp.add(post);
      });
      _notification = temp;
      notifyListeners();
      return true;
    } catch (e) {
      print("error getNotiifaction");
      print(e);
      return false;
    }
  }

  Future<void> addlike(int id) async {
    try {
      var token = storage.getItem('token');
      String url = 'http://10.0.2.2:8000/api/addlike/';
      http.Response response = await http.post(
        Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'token $token',
        },
        body: json.encode({
          "id": id,
        }),
      );
      var data = json.decode(response.body);
      if (data['error'] == false) {
        getPostData();
      }
    } catch (e) {
      print("error addlike");
      print(e);
    }
  }

  Future<void> addcomment(int postid, String commenttext) async {
    try {
      var token = storage.getItem('token');
      String url = 'http://10.0.2.2:8000/api/addcomment/';
      http.Response response = await http.post(
        Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'token $token',
        },
        body: json.encode({
          "postid": postid,
          "comment": commenttext,
        }),
      );
      var data = json.decode(response.body);
      if (data['error'] == false) {
        getPostData();
      }
    } catch (e) {
      print("error addcomment");
      print(e);
    }
  }

  Future<void> addreply(int commentid, String replytext) async {
    try {
      var token = storage.getItem('token');
      String url = 'http://10.0.2.2:8000/api/addreply/';
      http.Response response = await http.post(
        Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
          'Authorization': 'token $token',
        },
        body: json.encode({
          "commentid": commentid,
          "replytext": replytext,
        }),
      );
      var data = json.decode(response.body);
      // print(data);
      if (data['error'] == false) {
        getPostData();
      }
    } catch (e) {
      print("error addreply");
      print(e);
    }
  }

  Future<void> initParam() async {
    try {
      String url = 'http://10.0.2.2:8000/api/initParam/';
      var token = storage.getItem('token');
      http.Response response = await http.get(
        Uri.parse(url),
        headers: {
          'Authorization': 'token $token',
        },
      );
      var data = json.decode(response.body);
      User user = User.fromJson(data);
      storage.setItem('user', user.toJson());
    } catch (e) {
      print("iniit param");
      print(e);
    }
  }

  Future<bool> loginNow(String username, String password) async {
    try {
      String url = 'http://10.0.2.2:8000/api/login/';
      http.Response response = await http.post(
        Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
        },
        body: json.encode({
          'username': username,
          'password': password,
        }),
      );
      var data = json.decode(response.body) as Map;
      // print(data);
      if (data.containsKey('token')) {
        storage.setItem('token', data['token']);

        initParam();
        return false;
      }

      return true;
    } catch (e) {
      print("error loginnow");
      print(e);
      return true;
    }
  }

  Future<bool> registerNow(String username, String password) async {
    try {
      String url = 'http://10.0.2.2:8000/api/register/';
      http.Response response = await http.post(
        Uri.parse(url),
        headers: {
          "Content-Type": "application/json",
        },
        body: json.encode({
          'username': username,
          'password': password,
        }),
      );
      var data = json.decode(response.body) as Map;
      // if (data['error'] = false) {
      //   return false;
      // }
      return data['error'];
    } catch (e) {
      print("error register now");
      print(e);
      return true;
    }
  }

  List<Publication> get publication {
    if (_publications != null) {
      return [..._publications];
    }
    return [];
  }

  List<Notifications> get notifs {
    if (_notification != null) {
      return [..._notification];
    }
    return [];
  }
}
