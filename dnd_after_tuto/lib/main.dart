import 'package:dnd_after_tuto/screens/detail_pub.dart';
import 'package:dnd_after_tuto/screens/login.dart';
import 'package:dnd_after_tuto/post_state.dart';
import 'package:dnd_after_tuto/screens/home.dart';
import 'package:dnd_after_tuto/screens/register.dart';
import 'package:dnd_after_tuto/screens/news.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

const couleurPrincipale = Color.fromRGBO(197, 161, 68, 1);
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => PostState(),
      child: MaterialApp(
        title: 'Flutter Demo ',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Login(),
        routes: {
          Login.routeName: (ctx) => Login(),
          Home.routeName: (ctx) => Home(),
        },
      ),
    );
  }
}
